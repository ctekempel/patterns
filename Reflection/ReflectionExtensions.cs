﻿namespace ExmUnsub.Extensions
{
    using System.Reflection;

    public static class ReflectionExtensions
    {
        public static T InvokePrivateMethod<T>(this object o, string methodName, object[] methodparams) where T : class
        {
            MethodInfo dynMethod = o.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            return dynMethod.Invoke(o, methodparams) as T;
        }

        public static T InvokePrivateProperty<T>(this object o, string propertyName) where T : class
        {
            PropertyInfo dynMethod = o.GetType().GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance);
            return dynMethod.GetValue(o) as T;
        }   
    }
}