<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

// TODO: to enable the page, set enableUnlockButton = true;
private bool enableUnlockButton = true;

protected void Page_Load(object sender, EventArgs e)
{
    Session.Abandon();
}

</script>

<!DOCTYPE html>

<html>

<head>

    <title>Abandon Session Page</title>
    <link rel="shortcut icon" href="/sitecore/images/favicon.ico" />
    <style type="text/css">

    body
    {
        font-family: normal 11pt "Times New Roman", Serif;
  }

    .Warning
    {
        color: red;
    }

    </style>

</head>

<body>

<form runat="server" id="form">
    Session Abandoned
</form>

</body>

</html>