with ItemsWithFullPath (id, name
,fullpath
)
as
(
       select ID, Name, cast(name as nvarchar(max)) as fullpath
       from Items
       where ParentID = '00000000-0000-0000-0000-000000000000'
       union all
       select i.ID, i.Name, cast((fullpath +'/' + i.name) as nvarchar(max)) as fullpath
       from items i 
       inner join ItemsWithFullPath a on i.ParentID = a.id
)

select paths.fullpath, count(distinct c.id)
from items p
inner join ItemsWithFullPath paths on paths.id = p.id
inner join items c on p.id = c.ParentID
group by paths.fullpath
having count(distinct c.id) >= 100
order by count(distinct c.id) desc