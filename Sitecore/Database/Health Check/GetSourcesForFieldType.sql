with ItemsWithFullPath (id, name
,fullpath
)
as
(
       select ID, Name, cast(name as nvarchar(max)) as fullpath
       from Items
       where ParentID = '00000000-0000-0000-0000-000000000000'
       union all
       select i.ID, i.Name, cast((fullpath +'/' + i.name) as nvarchar(max)) as fullpath
       from items i 
       inner join ItemsWithFullPath a on i.ParentID = a.id
)



select iwfp.fullpath, ft.value,  fs.value
from ItemsWithFullPath iwfp 
inner join items i on i.id = iwfp.id
inner join fields ft on ft.ItemId = i.ID and ft.Value like 'treelist' and ft.FieldId = '{AB162CC0-DC80-4ABF-8871-998EE5D7BA32}' --  type field id
left outer join fields fs on fs.ItemId = i.ID and fs.FieldId = '{1EB8AE32-E190-44A6-968D-ED904C794EBF}' --  source field id
order by iwfp.fullpath