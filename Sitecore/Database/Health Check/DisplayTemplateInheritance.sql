with ItemsWithFullPath (id, name
,fullpath
)
as
(
       select ID, Name, cast(name as nvarchar(max)) as fullpath
       from Items
       where ParentID = '00000000-0000-0000-0000-000000000000'
       union all
       select i.ID, i.Name, cast((fullpath +'/' + i.name) as nvarchar(max)) as fullpath
       from items i 
       inner join ItemsWithFullPath a on i.ParentID = a.id
)


SELECT templatePath.fullpath as template, baseTemplatePath.fullpath as baseTemplate
FROM  
(
     SELECT ItemId,  
         CAST ('<M>' + REPLACE(Value, '|', '</M><M>') + '</M>' AS XML) AS Data  
     FROM  fields
	 where FieldId  = '{12C33F3F-86C5-43A5-AEB4-5598CEC45116}' --__Base Template field id
) AS baseTemplateIds CROSS APPLY Data.nodes ('/M') AS Split(baseTemplateIds)
inner join ItemsWithFullPath templatePath on templatePath.id = baseTemplateIds.ItemId
inner join ItemsWithFullPath baseTemplatePath on convert(varchar(100),baseTemplatePath.id) = REPLACE(REPLACE(Split.baseTemplateIds.value('.', 'VARCHAR(100)'),'{',''),'}','')
where templatePath.fullpath like 'sitecore/templates/User Defined/%' -- project folder path
and baseTemplatePath.fullpath like 'sitecore/templates/User Defined/%' -- project folder path
order by templatePath.fullpath, baseTemplatePath.fullpath
