<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

// TODO: to enable the page, set enableUnlockButton = true;
private bool enableUnlockButton = true;

protected void Page_Load(object sender, EventArgs e)
{
    this.unlockButton.Enabled = this.enableUnlockButton;
}

protected void unlockButton_Click(object sender, EventArgs e)
{
    if (!this.enableUnlockButton)
    {
        return;
    }

    var user = Membership.GetUser("sitecore\\admin");
    var tempPassword = user.ResetPassword();
    user.ChangePassword(tempPassword, "b");
    this.resultMessageLiteral.Visible = true;
    this.unlockButton.Visible = false;
}

protected void descriptionLiteral_PreRender(object sender, EventArgs e)
{
    this.descriptionLiteral.Visible = !this.enableUnlockButton;
}

</script>

<!DOCTYPE html>

<html>

<head>

    <title>Reset Administrator Password Page</title>
    <link rel="shortcut icon" href="/sitecore/images/favicon.ico" />
    <style type="text/css">

    body
    {
        font-family: normal 11pt "Times New Roman", Serif;
  }

    .Warning
    {
        color: red;
    }

    </style>

</head>

<body>

<form runat="server" id="form">

<asp:Literal
    runat="server"
    ID="descriptionLiteral"
    EnableViewState="false"
    OnPreRender="descriptionLiteral_PreRender">
    <p>This page is currently disabled.</p>
    <p>To enable the page, modify the ASPX page and set enableUnlockButton = true.</p>
</asp:Literal>
<asp:Literal
    runat="server"
    ID="resultMessageLiteral"
    Visible="false">
    <p>The Administrator user's password has been successfully reset.</p>
    <p class="Warning">Do not forget to set enableUnlockButton = false in the ASPX page again.</p>
</asp:Literal>
<asp:Button
    runat="server"
    ID="unlockButton"
    Text="Reset Admin Password"
    OnClick="unlockButton_Click"/>

</form>

</body>

</html>