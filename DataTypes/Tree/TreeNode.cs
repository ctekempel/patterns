namespace Tree.Model
{
    using System.Linq;

    public class TreeNode<T>
    {
        public TreeNode()
        {
            this.Children = new Tree<T>(Enumerable.Empty<TreeNode<T>>());
        }

        public TreeNode(T value)
        {
            this.Value = value;
            this.Children = new Tree<T>(Enumerable.Empty<TreeNode<T>>());
        }

        public T Value { get; set; }

        public Tree<T> Children { get; set; }

        public TreeNode<T> Parent { get; set; }
    }
}