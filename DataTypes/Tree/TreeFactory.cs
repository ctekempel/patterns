namespace Tree.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TreeFactory
    {
        public static Tree<TOutput> Create<TOutput, TInput>(
            IEnumerable<TInput> data,
            Func<TInput, object> parentKeySelector,
            Func<TInput, object> keySelector,
            Func<TInput, TOutput> valueSelector)
        {
            var nodeDictionary = new Dictionary<object, TreeNode<TOutput>>();

            foreach (var item in data)
            {
                var parentKey = parentKeySelector(item);
                var key = keySelector(item);
                var value = valueSelector(item);

                TreeNode<TOutput> treeNode;
                if (nodeDictionary.ContainsKey(key))
                {
                    treeNode = nodeDictionary[key];
                    treeNode.Value = value;
                }
                else
                {
                    treeNode = new TreeNode<TOutput>(value);
                    nodeDictionary.Add(key, treeNode);
                }

                if (parentKey != null)
                {
                    if (!nodeDictionary.ContainsKey(parentKey))
                    {
                        nodeDictionary.Add(parentKey, new TreeNode<TOutput>());
                    }

                    var parentNode = nodeDictionary[parentKey];
                    parentNode.Children.Add(treeNode);
                    treeNode.Parent = parentNode;
                }
            }

            foreach (var treeNode in nodeDictionary.Where(pair => pair.Value.Value == null))
            {
                foreach (var child in treeNode.Value.Children)
                {
                    child.Parent = null;
                }
            }

            return new Tree<TOutput>(nodeDictionary
                    .Where(pair => pair.Value.Value != null && pair.Value.Parent == null)
                    .Select(pair => pair.Value));
        }
    }
}