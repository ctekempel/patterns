namespace Tree.Model
{
    using System.Collections.Generic;

    public class Tree<T> : List<TreeNode<T>>
    {
        public Tree(IEnumerable<TreeNode<T>> enumerable)
        {
            this.AddRange(enumerable);
        }
    }
}